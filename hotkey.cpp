#include <iostream> // for input and output streams
#include <csignal>  // signal handling
#include <thread>
#include <ini.h>

#include "keyboard.hpp"
#include "device.hpp"
#include "arguments.hpp"
#include "logging.hpp"

bool killreceive = false;

void signalHandler(int) {
    if (killreceive) {
        std::cerr << "Hard killing myself." << std::endl;
        exit(EXIT_FAILURE);
    }

    INFO_STREAM << "Gracefully shutting down" << std::endl;
    killreceive = true;
}

static int ini_parser(void *, const char *section, const char *name,
                      const char *value) {
    devices *device = devices::getInstance();
    DEBUG_STREAM << "[" << section << "] " << name << " = " << value << std::endl;
    keyboard *k = device->get_device(section);

    if (k == NULL) {
        return 0; // 0 means failure
    }

    return k->add_config_option(name, value);
}

void start_threads(std::list<keyboard *> *keyboards) {
    arguments *args = arguments::getInstance();
    DEBUG_STREAM << "Parsing ini file " << args->get_configfile() << std::endl;
    int r = ini_parse(args->get_configfile().c_str(), ini_parser, NULL);

    if (r < 0) {
        std::cerr << "Could not open file " << args->get_configfile() << std::endl;
    }

    for (std::list<keyboard *>::iterator it = keyboards->begin();
            it != keyboards->end(); ++it) {
        if (args->should_print()) {
            std::string devicename = args->get_device();

            if (devicename.length() == 0 || devicename.compare((*it)->get_name()) == 0) {
                std::thread(&keyboard::print_input_codes, *it).detach();
            }
        } else {
            std::thread(&keyboard::parse_inputs, *it).detach();
        }
    }
}

int main(int argc, char **argv) {
    signal(SIGTERM, signalHandler);
    signal(SIGINT, signalHandler);
    arguments *args = arguments::getInstance();
    int ret = args->parse_args(argc, argv);

    if (ret) {
        return ret - 1;
    }

    if (args->should_print()) {
        INFO_STREAM << "Showing values of pressed keys." << std::endl;
        INFO_STREAM << "Press CTRL + C to quit" << std::endl;
    }

    devices *d = devices::getInstance();
    start_threads(d->get_devices());

    while (!killreceive) {
        // TODO Observe the config file for changes and reload.
        std::this_thread::sleep_for(std::chrono::milliseconds(300));
        std::list<keyboard *> found = d->scan_for_new_devices();

        if (!found.empty()) {
            start_threads(&found);
        }
    }

    return EXIT_SUCCESS;
}
