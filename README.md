# Hotkeys

Simple program to configure a keyboard as hotkeys

programs to execute on hotkey can be configured in an INI file

## Dependencies

### for execution

* inih

### for development

* googletest
* astyle

## Install

Esecute the following to compile the program:

```bash
mkdir build
cd build
cmake ..
make -j 4
```

now you can execute the program with ./hotkey

To get a more permanent usage install a systemd unit.
It is best to install it as a user uni and not a system unit.

So it should be in /etc/systemd/user/hotkey.service

```systemd
[Unit]
Description=Make use of hotkey keyboard

[Service]
Type=simple
ExecStart=/path/to/build/dir/hotkey

[Install]
WantedBy=multi-user.target
```

## Config file

The default keymap file is located at "~/.config/hotkeys.ini"

Define a section for each keyboard in your system with the devicename as the
sectionname.
It has one parameter, to disable normal input from the device.
If you use a secondary keyboard just for hotkeys, this might be set to 1

For all other options, use the keyid as the key and the command to be executed
as value.

### Example

```ini
[AT Translated Set 2 keyboard]
disable=0
49=/usr/bin/notify-send "Pressed hotkey"
73=/usr/bin/xdg-screensaver lock
18=keyelevate 500
19=keyelevate global 1000
44=/usr/bin/terminator -T "I like trains" -f -e "sleep 0.1 && sl"
549=/usr/bin/notify-send "Keycombo"
```

There is one special command "keyelevate" which will let you modify the keycodes.

This means, that for our example configuration, if the key with the id 18 is
pressed and hold, all other keyvalues will be increased by 500.

"keyelevate" normally only affects the keyboard it is pressed on. The modifier
"global" lets you instead apply it to all keyboards.

This could be useful, if you habe a keyboard with only one button and want to
use it to change the normal keyboard to be interpreted as hotkeys instead of
normal keys.

## LICENSE

This project is licensed under the MIT License. For details see LICENSE
