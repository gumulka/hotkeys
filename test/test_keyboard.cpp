
#include <gtest/gtest.h>
#include <thread>
#include <fstream>        // for file operations
#include <linux/input.h>  // defines the struct input_event
#include <ctime>

#include "keyboard.hpp"

#define INPUT_CODES_FILENAME "helper_hotkey_test"
#define INPUT_SECOND_FILENAME "helper_hotkey_second_test"
#define UNKNOWN_FILE "lekrjiasbvnlwearufkansd.fkawej-okasdnf"

std::string command;

void set_command(std::string c) {
    command = c;
}

void empty_command(std::string) {
}

void helper_code_writer(int *codes, int *values, int len,
                        const char *filename = INPUT_CODES_FILENAME) {
    try {
        std::ofstream device;
        device.open(filename, std::ios::out | std::ios::binary | std::ios::trunc);

        if (! device.is_open()) {
            std::cerr << "Could not open file \"" << filename << "\"" << std::endl;
            return;
        }

        for (int i = 0; i < len; i++) {
            struct input_event buffer;
            memset(&buffer, 0, sizeof(struct input_event));
            buffer.value = values[i];
            buffer.code = codes[i];
            device.write((char *) &buffer, sizeof(struct input_event));
        }

        device.close();
    } catch (std::exception &e) {
        std::cerr << "Could not write testfile" << std::endl;
    }
}

void helper_killer(keyboard *keyboard) {
    std::this_thread::sleep_for(std::chrono::milliseconds(200));
    keyboard->stop_listening();
}

TEST(keyboard, get_keyboard_name) {
    keyboard k(0, UNKNOWN_FILE);
    ASSERT_STREQ(k.get_name().c_str(), UNKNOWN_FILE);
}

TEST(keyboard, correct_config_option) {
    keyboard k(0, "");
    ASSERT_EQ(k.add_config_option("80", "/usr/bin/bash"), 1);
}

TEST(keyboard, wrong_config_option) {
    keyboard k(0, "");
    ASSERT_EQ(k.add_config_option("/usr/bin/bash", "80"), 0);
}

TEST(keyboard, simple_command) {
    int codes[30] = {47, 47};
    int values[30] = {1, 0};
    const char *value = "/testing/command";
    command = "";
    helper_code_writer(codes, values, 2);
    keyboard k(INPUT_CODES_FILENAME, "testkeyboard");
    ASSERT_EQ(k.add_config_option("47", value), 1);
    std::thread (helper_killer, &k).detach();
    k.parse_inputs_with_function(set_command);
    ASSERT_STREQ(command.c_str(), value);
}

TEST(keyboard, local_elevate_option) {
    int codes[30] = {90, 47, 47, 90};
    int values[30] = {1, 1, 0, 0};
    const char *value = "/testing/command";
    command = "";
    helper_code_writer(codes, values, 4);
    keyboard k(INPUT_CODES_FILENAME, "testkeyboard");
    ASSERT_EQ(k.add_config_option("90", "keyelevate 500"), 1);
    ASSERT_EQ(k.add_config_option("547", value), 1);
    std::thread (helper_killer, &k).detach();
    k.parse_inputs_with_function(set_command);
    ASSERT_STREQ(command.c_str(), value);
}

TEST(keyboard, local_elevate_only) {
    int codes[30] = {90, 55, 55, 90};
    int values[30] = {1, 1, 0, 0};
    helper_code_writer(codes, values, 1, "file1");
    helper_code_writer(codes + 1, values + 1, 2, "file2");
    const char *value = "/testing/command";
    command = "";
    keyboard k1("file1", "testkeyboard1");
    keyboard k2("file2", "testkeyboard2");
    ASSERT_EQ(k1.add_config_option("90", "keyelevate 800"), 1);
    ASSERT_EQ(k2.add_config_option("55", value), 1);
    std::thread (helper_killer, &k1).detach();
    k1.parse_inputs_with_function(empty_command);
    std::thread (helper_killer, &k2).detach();
    k2.parse_inputs_with_function(set_command);
    ASSERT_STREQ(command.c_str(), value);
}

TEST(keyboard, global_elevate_option) {
    int codes[30] = {90, 53, 53, 90};
    int values[30] = {1, 1, 0, 0};
    helper_code_writer(codes, values, 2, "file1");
    helper_code_writer(codes + 1, values + 1, 2, "file2");
    helper_code_writer(codes + 3, values + 3, 1, "file3");
    const char *value = "/testing/command";
    command = "";
    keyboard k1("file1", "testkeyboard1");
    keyboard k2("file2", "testkeyboard2");
    keyboard k3("file3", "testkeyboard3");
    ASSERT_EQ(k1.add_config_option("90", "keyelevate global 1000"), 1);
    ASSERT_EQ(k2.add_config_option("1053", value), 1);
    ASSERT_EQ(k3.add_config_option("90", "keyelevate global 1000"), 1);
    std::thread (helper_killer, &k1).detach();
    k1.parse_inputs_with_function(empty_command);
    std::thread (helper_killer, &k2).detach();
    k2.parse_inputs_with_function(set_command);
    ASSERT_STREQ(command.c_str(), value);
    command = "";
    std::thread (helper_killer, &k3).detach();
    k3.parse_inputs_with_function(empty_command);
    std::thread (helper_killer, &k2).detach();
    k2.parse_inputs_with_function(set_command);
    ASSERT_STREQ(command.c_str(), "");
}

TEST(keyboard, wrong_enable_config_option) {
    keyboard k(0, "");
    ASSERT_EQ(k.add_config_option("disable", "/usr/bin/bash"), 0);
}


TEST(keyboard, scriptdir) {
    keyboard k(0, "");
    ASSERT_EQ(k.add_config_option("scriptdir", "/usr/bin/"), 1);
}

TEST(keyboard, not_exixting_scriptdir) {
    keyboard k(0, "");
    ASSERT_EQ(k.add_config_option("scriptdir",
                                  "/kalkdsfjewlk/lkjalewrjadsf/lkjelkrklndsf/"), 0);
}

TEST(keyboard, simple_keyup_command) {
    int codes[30] = {47, 47};
    int values[30] = {1, 0};
    const char *value = "/testing/command";
    command = "";
    helper_code_writer(codes, values, 2, INPUT_CODES_FILENAME);
    keyboard k(INPUT_CODES_FILENAME, "testkeyboard");
    // ASSERT_EQ(k.add_config_option("47", UNKNOWN_FILE), 1);
    ASSERT_EQ(k.add_config_option("-47", value), 1);
    std::thread (helper_killer, &k).detach();
    k.parse_inputs_with_function(set_command);
    ASSERT_STREQ(command.c_str(), value);
}

#include "keyboard_device.hpp"

TEST(keyboard_device, unreadable_file) {
    struct timeval diff, startTV, endTV;
    gettimeofday(&startTV, NULL);
    EXPECT_ANY_THROW(keyboard_device(UNKNOWN_FILE));
    gettimeofday(&endTV, NULL);
    timersub(&endTV, &startTV, &diff);
    ASSERT_GT(diff.tv_sec, 5);
}
