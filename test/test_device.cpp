
#include <gtest/gtest.h>

#include "device.hpp"

//#cmakedefine VAR ...
#define PROCFILE_DIR "../test_assets/"
#define SINGLE_PROCFILE PROCFILE_DIR "procfile1"
#define DOUBLE_PROCFILE PROCFILE_DIR "procfile2"

TEST(device, is_same_instance) {
    devices *device = devices::getInstance(false);
    devices *second = devices::getInstance(false);
    ASSERT_EQ(device, second);
}

// this must be the first test, since we are working with a singleton.
TEST(device, unreadable_procfile) {
    devices *device = devices::getInstance(false);
    device->scan_for_new_devices("laksdjflwkenruasndfiasdfjasdflhasjn");
    ASSERT_EQ(device->get_num_devices(), 0);
}

TEST(device, first_proc_file) {
    devices *device = devices::getInstance(false);
    device->scan_for_new_devices(SINGLE_PROCFILE);
    ASSERT_EQ(device->get_num_devices(), 2);
}

TEST(device, second_proc_file) {
    devices *device = devices::getInstance(false);
    device->scan_for_new_devices(DOUBLE_PROCFILE);
    ASSERT_EQ(device->get_num_devices(), 3);
}

TEST(device, get_keyboards) {
    devices *device = devices::getInstance(false);
    device->scan_for_new_devices(DOUBLE_PROCFILE);
    ASSERT_EQ(device->get_num_devices(), 3);
    keyboard *first = device->get_device("AT Translated Set 2 keyboard");
    ASSERT_TRUE(first != NULL);
    keyboard *second = device->get_device("Asus WMI hotkeys");
    ASSERT_TRUE(second != NULL);
    keyboard *third = device->get_device("Logitech USB Keyboard");
    ASSERT_TRUE(third != NULL);
}

TEST(device, remove_keyboard) {
    devices *device = devices::getInstance(false);
    device->scan_for_new_devices(DOUBLE_PROCFILE);
    ASSERT_EQ(device->get_num_devices(), 3);
    keyboard *third = device->get_device("Logitech USB Keyboard");
    ASSERT_TRUE(third != NULL);
    device->scan_for_new_devices(SINGLE_PROCFILE);
    ASSERT_EQ(device->get_num_devices(), 2);
    third = device->get_device("Logitech USB Keyboard");
    ASSERT_TRUE(third == NULL);
}

TEST(device, add_keyboard) {
    devices *device = devices::getInstance(false);
    device->scan_for_new_devices(SINGLE_PROCFILE);
    ASSERT_EQ(device->get_num_devices(), 2);
    keyboard *third = device->get_device("Logitech USB Keyboard");
    ASSERT_TRUE(third == NULL);
    device->scan_for_new_devices(DOUBLE_PROCFILE);
    ASSERT_EQ(device->get_num_devices(), 3);
    third = device->get_device("Logitech USB Keyboard");
    ASSERT_TRUE(third != NULL);
}

TEST(device, return_add_keyboard) {
    devices *device = devices::getInstance(false);
    device->scan_for_new_devices(SINGLE_PROCFILE);
    std::list<keyboard *> added = device->scan_for_new_devices(DOUBLE_PROCFILE);
    ASSERT_EQ(added.size(), 1);
    keyboard *third = added.front();
    ASSERT_TRUE(third != NULL);
    ASSERT_STREQ(third->get_name().c_str(), "Logitech USB Keyboard");
}

TEST(device, return_remove_keyboard) {
    devices *device = devices::getInstance(false);
    device->scan_for_new_devices(DOUBLE_PROCFILE);
    std::list<keyboard *> added = device->scan_for_new_devices(SINGLE_PROCFILE);
    ASSERT_EQ(added.size(), 0);
}
