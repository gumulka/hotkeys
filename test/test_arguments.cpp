
#include <gtest/gtest.h>
#include "arguments.hpp"
#include "logging.hpp"
#include <getopt.h> // for getopts_long

TEST(argparse, current_folder_command_cleaner) {
    const char *longcommand = "./arguments";
    ASSERT_STREQ(get_cleaned_commandname(longcommand).c_str(), "arguments");
}

TEST(argparse, absolut_command_cleaner) {
    const char *longcommand = "/usr/bin/arguments";
    ASSERT_STREQ(get_cleaned_commandname(longcommand).c_str(), "arguments");
}

TEST(argparse, is_same_instance) {
    arguments *args = arguments::getInstance();
    arguments *second = arguments::getInstance();
    ASSERT_EQ(args, second);
}

TEST(argparse, print_help) {
    char *const argv[2] = {(char *)"test", (char *)"-h"};
    arguments *args = arguments::getInstance();
    ASSERT_EQ(args->parse_args(2, argv), 1);
}

TEST(argparse, print_help_long) {
    char *const argv[2] = {(char *)"test", (char *)"--help"};
    arguments *args = arguments::getInstance();
    ASSERT_EQ(args->parse_args(2, argv), 1);
}

TEST(argparse, config_file_without_param) {
    char *const argv[2] = {(char *)"test", (char *)"-c"};
    arguments *args = arguments::getInstance();
    ASSERT_EQ(args->parse_args(2, argv), 2);
}

TEST(argparse, config_file_with_params) {
    char *const argv[3] = {(char *)"test", (char *)"-c", (char *)"test.ini"};
    arguments *args = arguments::getInstance();
    ASSERT_EQ(args->parse_args(3, argv), 0);
    ASSERT_STREQ(args->get_configfile().c_str(), (char *)"test.ini");
}

TEST(argparse, make_quiet) {
    logger_set_log_level(LOG_LEVEL_INFO);
    char *const argv[2] = {(char *)"test", (char *)"-q"};
    arguments *args = arguments::getInstance();
    ASSERT_EQ(args->parse_args(2, argv), 0);
    ASSERT_FALSE(logger_will_log(LOG_LEVEL_TRACE));
    ASSERT_FALSE(logger_will_log(LOG_LEVEL_WARN));
}

TEST(argparse, increase_verbosity) {
    logger_set_log_level(LOG_LEVEL_INFO);
    char *const argv[2] = {(char *)"test", (char *)"-v"};
    arguments *args = arguments::getInstance();
    ASSERT_EQ(args->parse_args(2, argv), 0);
    ASSERT_TRUE(logger_will_log(LOG_LEVEL_DEBUG));
}

TEST(argparse, double_increase_verbosity) {
    logger_set_log_level(LOG_LEVEL_INFO);
    char *const argv[2] = {(char *)"test", (char *)"-vv"};
    arguments *args = arguments::getInstance();
    ASSERT_EQ(args->parse_args(2, argv), 0);
    ASSERT_TRUE(logger_will_log(LOG_LEVEL_DEBUG));
    ASSERT_TRUE(logger_will_log(LOG_LEVEL_TRACE));
}

TEST(argparse, quiet_overwriting_verbose) {
    logger_set_log_level(LOG_LEVEL_INFO);
    char *const argv[2] = {(char *)"test", (char *)"-vqv"};
    arguments *args = arguments::getInstance();
    ASSERT_EQ(args->parse_args(2, argv), 0);
    ASSERT_FALSE(logger_will_log(LOG_LEVEL_TRACE));
    ASSERT_FALSE(logger_will_log(LOG_LEVEL_WARN));
}

TEST(argparse, invalid_param) {
    char *const argv[2] = {(char *)"test", (char *)"-Y"};
    arguments *args = arguments::getInstance();
    ASSERT_EQ(args->parse_args(2, argv), 2);
}

TEST(argparse, extra_argument) {
    char *const argv[2] = {(char *)"test", (char *)"test"};
    arguments *args = arguments::getInstance();
    ASSERT_EQ(args->parse_args(2, argv), 2);
}

TEST(argparse, print_all_short) {
    char *const argv[2] = {(char *)"test", (char *)"-p"};
    arguments *args = arguments::getInstance();
    ASSERT_EQ(args->parse_args(2, argv), 0);
    ASSERT_TRUE(args->should_print());
    ASSERT_EQ(args->get_device(), "");
}

// Test both, because getopt behaves differently for long and short with optional arguments.
TEST(argparse, print_all_long) {
    char *const argv[2] = {(char *)"test", (char *)"--print"};
    arguments *args = arguments::getInstance();
    ASSERT_EQ(args->parse_args(2, argv), 0);
    ASSERT_TRUE(args->should_print());
    ASSERT_EQ(args->get_device(), "");
}

TEST(argparse, print_specific) {
    char *const argv[3] = {(char *)"test", (char *)"-p", (char *)"test"};
    arguments *args = arguments::getInstance();
    ASSERT_EQ(args->parse_args(3, argv), 0);
    ASSERT_TRUE(args->should_print());
    ASSERT_EQ(args->get_device(), "test");
}

// TODO test environment variable for HOTKEYS_CONFIG_FILE
