
#include "logging.hpp"
#include <map>

int global_log_level = LOG_LEVEL_INFO;

std::map<std::string, int> functions;
std::map<std::string, int> files;

bool logger_will_log(int level, const char *filename, const char *funcname ) {
    if (global_log_level == 0) {
        return false;
    }

    if (filename != NULL) {
        if (funcname != NULL) {
            std::string name = std::string(filename) + std::string(funcname);
            auto l = functions.find(name);

            if (l != functions.end()) {
                return level <= l->second;
            }
        }

        std::string name = std::string(filename);
        auto l = files.find(name);

        if (l != files.end()) {
            return level <= l->second;
        }
    }

    return level <= global_log_level;
}

void logger_set_log_level(int level, const char *filename,
                          const char *funcname) {
    if (filename != NULL) {
        if (funcname != NULL) {
            std::string name = std::string(filename) + std::string(funcname);
            functions[name] = level;
        } else {
            std::string name = std::string(filename);
            files[name] = level;
        }
    } else {
        global_log_level = level;
    }
}

void logger_increase_level() {
    if (global_log_level != 0) {
        global_log_level++;
    }
}

void logger_decrease_level() {
    if (global_log_level > 1) {
        global_log_level--;
    }
}
