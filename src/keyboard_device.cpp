#include "keyboard_device.hpp"
#include "logging.hpp"

#include <iostream>
#include <cstring>
#include <thread>

#define MAX_TRIES 30       // magic number, guessed out of the blue.
#define SLEEP_TIME_MS 200  // magic number, just guessed

keyboard_device::keyboard_device(std::string filename) {
    devicefile = filename;
    DEBUG_STREAM << "Opening device " << devicefile << std::endl;
    int tries = 0;

    // sometimes this method is called, before the device is ready in the filesystem
    // we have to make up for this fact, by polling for the file.
    while (tries < MAX_TRIES) {
        device.open(devicefile, std::ios::in | std::ios::binary);

        if (device.is_open()) {
            if (tries > 0) {
                DEBUG_STREAM << "Opened file " << devicefile << std::endl;
            }

            break;
        }

        if (tries == 0) {
            WARN_STREAM << "Problems opening file " << devicefile << std::endl;
        }

        tries++;
        std::this_thread::sleep_for(std::chrono::milliseconds(SLEEP_TIME_MS));
    }

    if (tries == MAX_TRIES ) {
        throw  std::runtime_error("Could not open file.");
    }
}

keyboard_device::~keyboard_device() {
    if (device.is_open()) {
        DEBUG_STREAM << "Closing device " << devicefile << std::endl;
        device.close();
    }
}

bool keyboard_device::is_readable() {
    return !closed && device.is_open();
}

struct input_event keyboard_device::get_next_event() {
    struct input_event buffer;
    memset(&buffer, 0, sizeof(struct input_event));
    get_next_event(&buffer);
    return buffer;
}

void keyboard_device::get_next_event(struct input_event *buffer) {
    // TODO check if the device has been closed while reading and throw exception.
    if (!closed) {
        device.read((char *) buffer, sizeof(struct input_event));
    }
}
