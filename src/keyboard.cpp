#include "keyboard.hpp"
#include <stdio.h>        // for snprintf()
#include <string.h>       //
#include <linux/input.h>  // defines the struct input_event
#include <iostream>       // for input and output streams
#include <fstream>        // for file operations
#include <iomanip>        // for setfill and setw
#include <thread>
#include <exception>
#if __cplusplus >= 201703L
#include <filesystem>
#else
#include <experimental/filesystem>
#endif

#include "logging.hpp"

#define VALUE_KEY_DOWN 1
#define VALUE_KEY_UP 0


// init static variables
int keyboard::global_elevator = 0;

keyboard::keyboard(int id, std::string name) {
    char devicefile[MAX_LEN];
    snprintf(devicefile, MAX_LEN - 1, "/dev/input/event%d", id);

    try {
        device = new keyboard_device(std::string(devicefile));
    } catch (const std::exception &e) {
        WARN_STREAM << e.what() << std::endl;
        device = NULL;
    }

    devicename = name;
}

keyboard::keyboard(std::string filename, std::string name) {
    try {
        device = new keyboard_device(filename);
    } catch (const std::exception &e) {
        WARN_STREAM << e.what() << std::endl;
        device = NULL;
    }

    devicename = name;
}

keyboard::~keyboard() {
    if (device != NULL) {
        delete device;
    }

    device = NULL;
}

#define MAX_CALL_LEN 512

int keyboard::set_status(std::string status) {
    char temp[MAX_CALL_LEN];
    snprintf(temp, MAX_CALL_LEN - 1, "xinput %s \"keyboard:%s\"", status.c_str(),
             devicename.c_str());

    if (system(temp)) {
        WARN_STREAM << "Could not " << status << " device " << devicename << std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

int keyboard::add_config_option(const char *name, const char *value) {
    const std::string DISABLE("disable");
    const std::string ELEVATE("keyelevate ");
    const std::string GLOBAL("global ");
    const std::string SCRIPTDIR("scriptdir");

    // parse disable option
    if (!DISABLE.compare(name)) {
        try {
            if (std::stoi(value)) {
                return ! set_status("disable");
            } else {
                return ! set_status("enable");
            }
        } catch (const std::invalid_argument &ia) {
            WARN_STREAM << "disable value must be 0 or 1 not " << value << std::endl;
            return 0;
        }
    }

    // parse scriptdir option
    if (!SCRIPTDIR.compare(name)) {
#if __cplusplus >= 201703L

        if (std::filesystem::is_directory(value)) {
#else

        if (std::experimental::filesystem::is_directory(value)) {
#endif
            scriptdir = std::string(value);
            return 1;
        } else {
            WARN_STREAM << SCRIPTDIR << " \"" << value << "\" is not a directory!" <<
                        std::endl;
            return 0;
        }
    }

    // parse keycode
    try {
        int key = std::stoi(name);
        std::string temp(value);

        // keycode value is an elevate command
        // best way to do startswith
        if (temp.rfind(ELEVATE, 0) == 0) {
            try {
                int elevate = std::stoi(temp.substr(temp.rfind(' ') + 1));

                if (temp.find(GLOBAL) != temp.npos) {
                    // found global string in options
                    globalkeyelevate[key] = elevate;
                    DEBUG_STREAM << "[" << devicename << "] key " << key <<
                                 " will globally elevate by " << elevate << std::endl;
                } else {
                    // not global, default to local
                    keyelevate[key] = elevate;
                    DEBUG_STREAM << "[" << devicename << "] key " << key <<
                                 " will locally elevate by " << elevate << std::endl;
                }
            } catch (const std::invalid_argument &ia) {
                WARN_STREAM << "error when parsing " << value << std::endl;
                return 0;
            }
        } else {
            // Normal behaviour.
            keymap[key] = std::string(value);
            DEBUG_STREAM << "[" << devicename << "] key " << key << " will be mapped to " <<
                         value << std::endl;
        }
    } catch (const std::invalid_argument &ia) {
        WARN_STREAM << "Could not parse " << name << std::endl;
        return 0;
    }

    return 1; // 1 means success for this function
}

void keyboard::execute_command(std::string command) {
    INFO_STREAM << "Executing: " << command << std::endl;

    if (system(command.c_str())) {
        WARN_STREAM << "Error while executing " << command << std::endl;
    }
}

void keyboard::parse_inputs_with_function( void (*f)(std::string)) {
    if (keymap.empty() && globalkeyelevate.empty() && scriptdir.length() == 0) {
        DEBUG_STREAM << "Keymap is empty" << std::endl;
        return;
    }

    while (device != NULL && device->is_readable()) {
        try {
            struct input_event buffer = device->get_next_event();

            if (buffer.code != 0) {
                DEBUG_STREAM << "[" << devicename << "] Got " << buffer.code << " - " <<
                             buffer.value;
                DEBUG_STREAM << " elevate values: " << local_elevator << " - " <<
                             global_elevator <<
                             std::endl;
            }

            if ((buffer.value == VALUE_KEY_DOWN || buffer.value == VALUE_KEY_UP) &&
                    buffer.code != 0) {
                int total = buffer.code + local_elevator + global_elevator;
                int sign =
                    1;  //!< value describing the sign for adding or substracting values to elevate. Must be plus or minus 1

                if (buffer.value == VALUE_KEY_UP) {
                    // negate the values for key up.
                    // Since we are only handling keyup and keydown events, this is possible.
                    // otherwise, this should be more sophisticated.
                    total = -total;
                    sign = -1;
                }

                std::map<int, std::string>::iterator command = keymap.find(total);
                std::map<int, int>::iterator global = globalkeyelevate.find(buffer.code);
                std::map<int, int>::iterator local = keyelevate.find(buffer.code);

                if (command != keymap.end()) {
                    std::thread (*f, command->second).detach();
                }

                if (scriptdir.length() != 0) {
                    std::string script = scriptdir + std::to_string(total);
                    DEBUG_STREAM << "checking for file " << script << std::endl;

                    if ( // not nice, but don't know better at the moment
#if __cplusplus >= 201703L
                        std::filesystem::exists(script)
#else
                        std::experimental::filesystem::exists(script)
#endif
                    ) {
                        std::thread (*f, script).detach();
                    }
                }

                if (local != keyelevate.end()) {
                    local_elevator += local->second * sign;

                    if (buffer.value == VALUE_KEY_DOWN) {
                        DEBUG_STREAM  << "[" << devicename << "] Increasing local" << std::endl;
                    } else {
                        DEBUG_STREAM  << "[" << devicename << "] Decreasing local" << std::endl;
                    }
                }

                if (global != globalkeyelevate.end()) {
                    global_elevator += global->second * sign;

                    if (buffer.value == VALUE_KEY_DOWN) {
                        DEBUG_STREAM << "[" << devicename << "] Increasing global" << std::endl;
                    } else {
                        DEBUG_STREAM << "[" << devicename << "] Decreasing global" << std::endl;
                    }
                }
            }

            if (buffer.code != 0) {
                DEBUG_STREAM << "[" << devicename << "] Elevate options after evaluating: ";
                DEBUG_STREAM << " elevate values: " << local_elevator << " - " <<
                             global_elevator <<
                             std::endl;
            }
        } catch (std::exception &e) {
            WARN_STREAM  << "[" << devicename << "] caught Exception" << std::endl;
            break;
        }
    }

    DEBUG_STREAM  << "[" << devicename << "] Finished parsing input" << std::endl;
}

void keyboard::parse_inputs() {
    parse_inputs_with_function(execute_command);
}

void keyboard::print_input_codes() {
    while (device != NULL && device->is_readable()) {
        struct input_event buffer = device->get_next_event();

        if (buffer.value == VALUE_KEY_DOWN && buffer.code != 0) {
            int total = buffer.code + local_elevator + global_elevator;
            std::map<int, std::string>::iterator command = keymap.find(total);
            std::map<int, int>::iterator global = globalkeyelevate.find(buffer.code);
            std::map<int, int>::iterator local = keyelevate.find(buffer.code);
            std::cout << "Device " << devicename << " received code " << buffer.code;

            if (total != buffer.code) {
                std::cout << " (modified to " << total << ")";
            }

            if (command != keymap.end()) {
                std::cout << " configured command: " <<  command->second;
            }

            if (local != keyelevate.end()) {
                local_elevator += local->second;
                std::cout << " value modifier: " << local->second;
            }

            if (global != globalkeyelevate.end()) {
                global_elevator += global->second;
                std::cout << " global value modifier: " << local->second;
            }

            std::cout << std::endl;
        } else if (buffer.value == VALUE_KEY_UP && buffer.code != 0) {
            std::map<int, int>::iterator global = globalkeyelevate.find(buffer.code);
            std::map<int, int>::iterator local = keyelevate.find(buffer.code);

            if (local != keyelevate.end()) {
                local_elevator -= local->second;
            }

            if (global != globalkeyelevate.end()) {
                global_elevator -= global->second;
            }
        }
    }
}
