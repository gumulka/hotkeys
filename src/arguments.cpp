#include <getopt.h> // for getopts_long
#include <pwd.h>    // for getting the home dir
#include <unistd.h> // for getting the home dir
#include <iostream> // for print
#include <mutex>    // for locking

#include "arguments.hpp"
#include "logging.hpp"

std::string get_cleaned_commandname(const char *name) {
    std::string temp(name);
    auto last = temp.rfind('/');

    if (last != std::string::npos) {
        temp = temp.substr(last + 1);
    }

    return temp;
}

void arguments::print_help(char *name) {
    std::cout << "Usage: " << get_cleaned_commandname(name) << " [params]" <<
              std::endl
              << "   -h, --help             print this help message" << std::endl
              << "   -v, --verbose          increase log output" << std::endl
              << "   -q, --quiet            don't print anything." << std::endl
              << "   -c, --config=FILENAME  use FILENAME as the config file." << std::endl
              << "   -p, --print[=NAME]     print keycodes [only of NAME] to terminal." <<
              std::endl;
}

std::mutex arguments_singleton;
arguments *arguments::instance = NULL;

arguments *arguments::getInstance() {
    if (instance != NULL) {
        return instance;
    }

    arguments_singleton.lock();

    // another thread could have created it while we were not locked.
    if (instance == NULL) {
        instance = new arguments();
    }

    arguments_singleton.unlock();
    return instance;
}

arguments::arguments() {
    const char *homedir;

    if ((homedir = getenv("HOTKEYS_CONFIG_FILE")) == NULL) {
        if ((homedir = getenv("HOME")) == NULL) {
            homedir = getpwuid(getuid())->pw_dir;
        }

        configfile = std::string(homedir);
        configfile.append("/.config/hotkeys.ini");
    } else {
        DEBUG_STREAM << "Found environment variable HOTKEYS_CONFIG_FILE=" << homedir <<
                     std::endl;
        configfile = std::string(homedir);
    }

    print = false;
    device = "";
}

int arguments::parse_args(int argc, char *const *argv) {
    int c;
    int option_index = 0;
    optind = 0; // reset getopt long.
    static struct option long_options[] = {
        {"help", no_argument, 0, 'h'},
        {"verbose", no_argument, 0, 'v'},
        {"quiet", no_argument, 0, 'q'},
        {"config", required_argument, 0, 'c'},
        {"print", optional_argument, 0, 'p'},
        {0, 0, 0, 0}
    };
    const char *options = ":hvqc:p:";

    while (true) {
        c = getopt_long(argc, argv, options, long_options, &option_index);
        TRACE("getopts_long parsed option %c (%d)\n", c, c);

        // optional arguments
        // getopt_long has problems with optional arguments and passes a ':' if there is none
        if (c == ':') {
            switch (optopt) {
            case 'p':
                c = 'p';
                break;

            default:
                break;
            }
        }

        if (c == -1) {
            break;
        }

        switch (c) {
        case 'h':
            print_help(argv[0]);
            return 1; // 1 = terminate success, 2 = terminate failure

        case 'v':
            logger_increase_level();
            break;

        case 'q':
            DISABLE_LOGGING;
            break;

        case 'c':
            configfile = std::string(optarg);
            break;

        case 'p':
            if (optarg) {
                device = std::string(optarg);
            }

            print = true;
            break;

        case ':':
            /* missing option argument */
            WARN("%s: option '-%c' requires an argument\n",
                 argv[0], optopt);
            return 2; // 1 = terminate success, 2 = terminate failure
            break;

        case '?':
        default:
            /* invalid option */
            WARN("%s: option '-%c' is invalid!\n",
                 argv[0], optopt);
            return 2; // 1 = terminate success, 2 = terminate failure
        }
    }

    if (optind < argc) {
        for (int i = optind; i < argc; i++) {
            WARN("%s: option '%s' is invalid!\n",
                 argv[0], argv[i]);
        }

        return 2; // 1 = terminate success, 2 = terminate failure
    }

    return 0; // 0 = continue with program
}
