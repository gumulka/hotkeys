#include <iostream>       // for input and output streams
#include <fstream>        // for file operations
#include <regex>          // For regex search
#include <mutex>          // for locking
#include "device.hpp"
#include "logging.hpp"

keyboard *get(std::list<keyboard *> list, std::string name) {
    for (std::list<keyboard *>::iterator it = list.begin();
            it != list.end(); ++it) {
        if ((*it)->get_name().compare(name) == 0) {
            return *it;
        }
    }

    return NULL;
}

std::mutex device_singleton;
std::mutex device_scan;
devices *devices::instance = NULL;

devices *devices::getInstance(bool init) {
    if (instance != NULL) {
        return instance;
    }

    device_singleton.lock();

    // another thread could have created it while we were not locked.
    if (instance == NULL) {
        instance = new devices(init);
    }

    device_singleton.unlock();
    return instance;
}

devices::devices(bool init) {
    if (init) {
        scan_for_new_devices();
    }

    INFO_STREAM << "Found " << keyboards.size() << " devices." << std::endl;
}

/** Check if a keyboard has the valid flag set and returns the invers.
 *
 * This will delete all non-valid keyboards as std::vector remove_if does not delete entries,
 * but just removes the links from its entry list.
 */
bool not_valid_anymore(const keyboard *k) {
    if (!k->still_valid) {
        INFO_STREAM << "Removing keyboard " << k->get_name() << std::endl;
        delete k;
    }

    return ! k->still_valid;
}

std::list<keyboard *> devices::scan_for_new_devices(const char *filename) {
    device_scan.lock();

    // mark all devices for deletion
    for (std::list<keyboard *>::iterator it = keyboards.begin();
            it != keyboards.end(); ++it) {
        (*it)->still_valid = false;
    }

    std::list<keyboard *> found;
    std::ifstream device;
    device.open(filename, std::ios::in);

    if (! device.is_open()) {
        WARN_STREAM << "Could not open proc file " << filename << std::endl;
        device_scan.unlock();
        return found;
    }

    DEBUG_STREAM << "Opened proc file " << filename << std::endl;
    std::string line;
    int latest_event = -1;
    std::string name = "";
    std::smatch m;
    std::regex e ("1[02]001[3Ff]");   // matches keyboard codes

    while (std::getline(device, line)) {
        // best way in c++ to do "starts with" without using huge libraries. (boost)
        if (line.rfind("N: Name", 0) == 0) {
            int startpos = line.find('"') + 1;
            int endpos = line.rfind('"');
            name = line.substr(startpos, endpos - startpos);
            TRACE_STREAM << "Found Name " << name << std::endl;
        }

        if (line.rfind("H: Handlers", 0) == 0) {
            int start = line.find("event", 11); // 11 = lenght of H: Handlers
            TRACE_STREAM << "Found Handlers " << line << std::endl;

            if (start > 0) {
                start += 5; // add length of event
                int end = line.find(" ", start);

                if (end > 0) {
                    latest_event = std::stoi(line.substr(start, end - start));
                } else {
                    latest_event = std::stoi(line.substr(start));
                }
            }
        }

        if (line.rfind("B: EV", 0) == 0) {
            if (latest_event >= 0 && std::regex_search(line, m, e)) {
                keyboard *board = get(keyboards, name);

                if (board == NULL) {
                    INFO_STREAM << "Adding keyboard " << latest_event << " with name \"" << name <<
                                '"' << std::endl;
                    keyboard *a = new keyboard(latest_event, name);
                    keyboards.push_back(a);
                    found.push_back(a);
                } else {
                    board->still_valid = true;
                }

                // reset the values.
                latest_event = -1;
                name = "";
            }
        }
    }

    device.close();
    keyboards.remove_if(not_valid_anymore);
    device_scan.unlock();
    return found;
}


devices::~devices() {
    for (std::list<keyboard *>::iterator it = keyboards.begin();
            it != keyboards.end(); ++it) {
        (*it)->stop_listening();
        delete (*it);
    }
}

int devices::get_num_devices() {
    return keyboards.size();
}

keyboard *devices::get_device(std::string name) {
    for (std::list<keyboard *>::iterator it = keyboards.begin();
            it != keyboards.end(); ++it) {
        if ((*it)->get_name().compare(name) == 0) {
            return *it;
        }
    }

    return NULL;
}
