#ifndef LOGGER_HPP
#define LOGGER_HPP 1

#include <iostream>

bool logger_will_log(int level, const char *filename = NULL,
                     const char *funcname = NULL);
void logger_set_log_level(int level, const char *filename = NULL,
                          const char *funcname = NULL);

void logger_increase_level();
void logger_decrease_level();

#define LOG_LEVEL_TRACE 5
#define LOG_LEVEL_DEBUG 4
#define LOG_LEVEL_INFO  3
#define LOG_LEVEL_WARN  2
#define LOG_LEVEL_ERROR 1

#define SET_LOG_LEVEL_TRACE logger_set_log_level(LOG_LEVEL_TRACE );
#define SET_LOG_LEVEL_DEBUG logger_set_log_level(LOG_LEVEL_DEBUG );
#define SET_LOG_LEVEL_INFO  logger_set_log_level(LOG_LEVEL_INFO  );
#define SET_LOG_LEVEL_WARN  logger_set_log_level(LOG_LEVEL_WARN  );
#define SET_LOG_LEVEL_ERROR logger_set_log_level(LOG_LEVEL_ERROR );

#define DISABLE_LOGGING logger_set_log_level(0);

#define SET_FILE_LOG_LEVEL_TRACE logger_set_log_level(LOG_LEVEL_TRACE , __FILE__);
#define SET_FILE_LOG_LEVEL_DEBUG logger_set_log_level(LOG_LEVEL_DEBUG , __FILE__);
#define SET_FILE_LOG_LEVEL_INFO  logger_set_log_level(LOG_LEVEL_INFO  , __FILE__);
#define SET_FILE_LOG_LEVEL_WARN  logger_set_log_level(LOG_LEVEL_WARN  , __FILE__);
#define SET_FILE_LOG_LEVEL_ERROR logger_set_log_level(LOG_LEVEL_ERROR , __FILE__);

#define SET_FUNCTION_LOG_LEVEL_TRACE logger_set_log_level(LOG_LEVEL_TRACE , __FILE__, __FUNCTION__);
#define SET_FUNCTION_LOG_LEVEL_DEBUG logger_set_log_level(LOG_LEVEL_DEBUG , __FILE__, __FUNCTION__);
#define SET_FUNCTION_LOG_LEVEL_INFO  logger_set_log_level(LOG_LEVEL_INFO  , __FILE__, __FUNCTION__);
#define SET_FUNCTION_LOG_LEVEL_WARN  logger_set_log_level(LOG_LEVEL_WARN  , __FILE__, __FUNCTION__);
#define SET_FUNCTION_LOG_LEVEL_ERROR logger_set_log_level(LOG_LEVEL_ERROR , __FILE__, __FUNCTION__);

#define TRACE_STREAM  if (!logger_will_log(LOG_LEVEL_TRACE , __FILE__, __FUNCTION__)) {} else std::cout
#define DEBUG_STREAM  if (!logger_will_log(LOG_LEVEL_DEBUG , __FILE__, __FUNCTION__)) {} else std::cout
#define INFO_STREAM   if (!logger_will_log(LOG_LEVEL_INFO  , __FILE__, __FUNCTION__)) {} else std::cout
#define WARN_STREAM   if (!logger_will_log(LOG_LEVEL_WARN  , __FILE__, __FUNCTION__)) {} else std::cerr
#define ERROR_STREAM  if (!logger_will_log(LOG_LEVEL_ERROR , __FILE__, __FUNCTION__)) {} else std::cerr

#define TRACE(...)  if (logger_will_log(LOG_LEVEL_TRACE , __FILE__, __FUNCTION__)) printf(__VA_ARGS__)
#define DEBUG(...)  if (logger_will_log(LOG_LEVEL_DEBUG , __FILE__, __FUNCTION__)) printf(__VA_ARGS__)
#define INFO(...)   if (logger_will_log(LOG_LEVEL_INFO  , __FILE__, __FUNCTION__)) printf(__VA_ARGS__)
#define WARN(...)   if (logger_will_log(LOG_LEVEL_WARN  , __FILE__, __FUNCTION__)) fprintf(stderr,  __VA_ARGS__)
#define ERROR(...)  if (logger_will_log(LOG_LEVEL_ERROR , __FILE__, __FUNCTION__)) fprintf(stderr,  __VA_ARGS__)

#endif
