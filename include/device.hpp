#ifndef DEVICE_HPP
#define DEVICE_HPP 1

#include <list>

#include "keyboard.hpp"


#define PROCFILE "/proc/bus/input/devices"

class devices {
  public:

    static devices *getInstance(bool init = true);

    /** returns the number of found keyboard devivies.
     *
     * \return number of found keyboards
     */
    int get_num_devices();

    /** Scans for newly plugged in devices.
     *
     * \return Returns the number of newly found devices.
     */
    std::list<keyboard *> scan_for_new_devices(const char *filename = PROCFILE);

    /** Returns the keyboard with the specified name.
     *
     * If the keyboard does not exists. NULL will be returned.
     *
     * \return keyboard with the specified name or NULL
    */
    keyboard *get_device(std::string name);

    /** Returns a list of all keyboard devices
     *
     * \return List of all keyboard devices.
    */
    std::list<keyboard *> *get_devices() {
        return &keyboards;
    }

  private:
    std::list<keyboard *> keyboards;
    static devices *instance;

    /** Reads in procinfo and parses all keyboards.
     *
     * The constructur parses the file "/proc/bus/input/devices" and extracts
     * information about all keyboards from there.
     * The parsed input event id's are used to construct keyboard classes and saved in a list.
     */
    devices(bool init);
    ~devices();
};

#endif
