#ifndef KEYBOARD_DEVICE_HPP
#define KEYBOARD_DEVICE_HPP 1

#include <linux/input.h>  // defines the struct input_event
#include <fstream>        // for file operations

/** Class representing physical keyboard. */
class keyboard_device {
  public:

    /** Constructor with device-file.
     *
     * The devicefile must point to a which only contains input events.
     * Otherwise, the behaviour of this class is undefined.
     */
    keyboard_device(std::string devicefile);
    ~keyboard_device();

    /* closes the device and stops all read operations. */
    void stop_listening() {
        closed = true;
    }

    /** checks if the device can be accessed. */
    bool is_readable();

    /** returns the next input event from device */
    struct input_event get_next_event();
    void get_next_event(struct input_event *buffer);

  private:
    std::ifstream device;
    std::string devicefile;
    bool closed = false;
};

#endif
