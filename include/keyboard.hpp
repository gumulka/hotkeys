#ifndef KEYBOARD_HPP
#define KEYBOARD_HPP 1

#include "keyboard_device.hpp"

#include <map>

#define MAX_LEN 25  // device name length is 17, so even 21 should be enough, but better be safe

/** Class representing physical keyboard. */
class keyboard {
  public:

    bool still_valid = true;

    /** Default constructor.
     *
     * This should be used to create a keyboard.
     * The deviceid must be a valid input event id.
     *
     * The devicename can be any name and is only used for debug output.
     *
     * \param deviceid ID of the eventsource.
     * \param devicename Humane readable name of the device.
     */
    keyboard(int deviceid, std::string devicename);
    keyboard(std::string filename, std::string devicename);
    ~keyboard();

    /** Print the id of all observed input events to STDOUT.
     *
     * This will not perform any hotkey actions, but only print out the code.
     */
    void print_input_codes();

    /** Observe the input event file and execute actions.
     *
     * This method will execute all hotkeys defined by add_config_options for
     * observed input events in a new thread.
     *
     * This is a blocking function and will not return.
     *
     * \sa add_config_option
     */
    void parse_inputs();
    void parse_inputs_with_function( void (*f)(std::string));

    /** Add a new hotkey command.
     *
     * Valid parameters for name are disable and any positive integer value in string representation.
     * If the name is "disable", the value must be either "0" or "1".
     * If the name is a number, the value must be a command to be executed.
     *
     * \param name Keyid or "disable"
     * \param value Command to be executed or "0" or "1".
     * \return 1 if successfull, 0 otherwise
     */
    int add_config_option(const char *name, const char *value);

    /** Stops the listening for events.
     *
     * \sa observe print_input_codes
     */
    void stop_listening() {
        if (device != NULL) {
            device->stop_listening();
        }
    }

    /** Get the human readable name of the device. */
    std::string get_name() const {
        return devicename;
    };

    void set_keyboard_device(keyboard_device *keydevice) {
        if (device != NULL) {
            delete device;
        }

        device = keydevice;
    }

  protected:
    int set_status(std::string status);
    static void execute_command(std::string command);

  private:
    std::string scriptdir;
    std::string devicename;
    keyboard_device *device = NULL;
    static int global_elevator;
    int local_elevator = 0;
    std::map<int, std::string> keymap;
    std::map<int, int> keyelevate;
    std::map<int, int> globalkeyelevate;
};

#endif
