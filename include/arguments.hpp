#ifndef ARGPARSE_HPP
#define ARGPARSE_HPP 1

#include <string>

/** class for command line arguments */
class arguments {
  public:

    static arguments *getInstance();

    /** Parse the command line arguments.
         *
         * Parses the command line arguments and fills the struct arguments with parsed values.
         * Also calls print_help if it assumes the user needs help.
         *
         * It will print out an error message if there is anything wrong. and return 2
         *
         * /param argc Number of command line arguments
         * /param argv List of command line arguments
         * /return 0 normally, 1 if program should terminate successfully, 2 on error.
         */
    int parse_args(int argc, char *const *argv);

    /** prints out a useful help and usage message.
         *
         * /param name The name of the program.
         */
    void print_help(char *name);

    bool should_print() {
        return print;
    }
    std::string get_configfile() {
        return configfile;
    }
    std::string get_device() {
        return device;
    }

  private:
    arguments();
    static arguments *instance;
    bool print;             //!< If only to print out messages and not execute the hotkeys
    std::string configfile; //!< Path of the config file
    std::string device;     //!< Name of device if print is set.
};

std::string get_cleaned_commandname(const char *name);
#endif
